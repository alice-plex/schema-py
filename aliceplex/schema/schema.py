import json
from enum import Enum
from pathlib import Path
from typing import Any

from jsonschema import RefResolver
from pkg_resources import resource_filename

__all__ = ["get_ref_resolver", "get_schema", "Schema"]


def init_store():
    schema_dir = resource_filename("aliceplex.schema", "data")
    files = Path(schema_dir).glob("*.schema.json")
    for file in files:
        schema = json.load(open(file, "r", encoding="utf-8"))
        store[schema["$id"]] = schema


store = {}
init_store()
resolver = RefResolver("", {}, store=store)


class Schema(Enum):
    Actor = "https://aliceplex-schema.joshuaavalon.app/v1/actor.schema.json"
    Album = "https://aliceplex-schema.joshuaavalon.app/v1/album.schema.json"
    Artist = "https://aliceplex-schema.joshuaavalon.app/v1/artist.schema.json"
    Episode = \
        "https://aliceplex-schema.joshuaavalon.app/v1/episode.schema.json"
    Movie = "https://aliceplex-schema.joshuaavalon.app/v1/movie.schema.json"
    Show = "https://aliceplex-schema.joshuaavalon.app/v1/show.schema.json"


def get_ref_resolver() -> RefResolver:
    return resolver


def get_schema(schema: Schema) -> Any:
    return store[schema.value]
